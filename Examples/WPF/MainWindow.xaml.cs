﻿using SaveFileDialogWithEncoding;
using System.IO;
using System.Windows;

namespace WPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            txtFile.Text = "Codificação";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtFile.Text))
            {
                MessageBox.Show("Enter text to encode.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SaveFileDialogEncoding saveFile = new SaveFileDialogEncoding
            {
                DefaultExt = "txt",
                EncodingType = EncodingType.UTF8,
                FileName = "sample",
                Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"
            };
            if (saveFile.ShowDialog())
            {
                using (StreamWriter sw = new StreamWriter(saveFile.FileName, false, saveFile.GetEncoding()))
                    sw.WriteLine(txtFile.Text);
            }
        }
    }
}
