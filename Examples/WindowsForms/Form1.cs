﻿using SaveFileDialogWithEncoding;
using System;
using System.IO;
using System.Windows.Forms;

namespace WindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txtFile.Text = "Codificação";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtFile.Text))
            {
                MessageBox.Show("Enter text to encode.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SaveFileDialogEncoding saveFile = new SaveFileDialogEncoding
            {
                DefaultExt = "txt",
                EncodingType = EncodingType.UTF8,
                FileName = "sample",
                Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"
            };
            if (saveFile.ShowDialog())
            {
                using (StreamWriter sw = new StreamWriter(saveFile.FileName, false, saveFile.GetEncoding()))
                    sw.WriteLine(txtFile.Text);
            }
        }
    }
}
