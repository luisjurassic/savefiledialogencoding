﻿namespace SaveFileDialogWithEncoding
{
    public enum EncodingType
    {
        ANSI,
        ASCII,
        BigEndianUnicode,
        Unicode,
        UTF8 
    };
}
